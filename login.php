<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from razonartificial.com/themes/reason/v1.4.5/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Nov 2015 14:35:09 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>Servicios de rastreo | Gps </title>

    <link rel="shortcut icon" href="favicon.ico" />

    <meta name="description" content="">




    <link href="assets/css/vendors.css" rel="stylesheet">

    <link href="assets/css/style-orange2.css" rel="stylesheet" title="default">
    <link href="assets/css/style/style.css" rel="stylesheet" >

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
    <![endif]-->
</head>


<body>


<div id="sb-site">
<div class="boxed">

<?php
    $nosotros = true ;
    include_once 'menu.php' ;
?>

<header class="main-header sm-header">
        <div class="container">
            <h4 class="page-title">
            <p class="text-center animated bounceInLeft animation-delay-12">
            Ingresa a Nuestra Plataforma
            </p>
            </h4>

        </div>
    </header>

<div ng-app="app">
    <div ui-view>

        </div>

</div>


<!-- <div class="container">
    <div class="center-block logig-form">
        <div class="panel panel-primary">
            <div class="panel-heading">Login : Servicio de Rastreo GPS </div>
            <div class="panel-body">
                <form role="form">
                    <div class="form-group">
                        <div class="input-group login-input">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" class="form-control" placeholder="Username">
                        </div>
                        <br>
                        <div class="input-group login-input">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input type="password" class="form-control" placeholder="Password">
                        </div>
                        <div class="checkbox">
                            <input type="checkbox" id="checkbox_remember">
                            <label for="checkbox_remember">Remember me</label>
                        </div>
                        <button type="submit" class="btn btn-ar btn-primary pull-right">Login</button>

                       <div class="clearfix"></div>
                        <hr class="dotted margin-10">
                        <a href="#" class="btn btn-ar btn-success pull-right">registrate</a>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
 -->





<?php include_once 'footer.php' ;?>

</div> <!-- boxed -->
</div> <!-- sb-site -->

<div id="back-top">
    <a href="#header"><i class="fa fa-chevron-up"></i></a>
</div>



<script src="assets/js/vendors.js"></script>

        <script src="http://www.corptigersoft.com.pe/gps/lib/jquery/jquery-1.11.2.min.js"></script>
        <script src="http://www.corptigersoft.com.pe/gps/lib/angular/angular.min.js"></script>
        <script src="http://www.corptigersoft.com.pe/gps/lib/angular/angular-ui-router.min.js"></script>
        <script src="http://www.corptigersoft.com.pe/gps/lib/angular/ui-bootstrap-custom-0.12.1.js"></script>
        <script src="http://www.corptigersoft.com.pe/gps/lib/angular/ui-bootstrap-custom-tpls-0.12.1.js"></script>
        <script src="http://www.corptigersoft.com.pe/gps/lib/socket.io/node_modules/socket.io-client/socket.io.js"></script>

        <!-- <link rel="stylesheet" type="text/css" media="screen" href="css/maqueta.min.css"> -->
        <script src="http://www.corptigersoft.com.pe/gps/js/app.min.js"></script>
        <script src="http://www.corptigersoft.com.pe/gps/js/controllers/controller.min.js"></script>
        <script src="http://www.corptigersoft.com.pe/gps/js/service/service.min.js"></script>
        <script src="http://www.corptigersoft.com.pe/gps/js/factory/factory.min.js"></script>
        <script src="http://www.corptigersoft.com.pe/gps/js/directives/eventoDirective.js"></script>
        <script src="http://www.corptigersoft.com.pe/gps/js/directives/helperDirective.js"></script>
        <script src="http://www.corptigersoft.com.pe/gps/js/directives/mapaDirective.js"></script>

</body>
</html>
