<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from razonartificial.com/themes/reason/v1.4.5/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Nov 2015 14:35:09 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>Servicios de rastreo | Gps </title>

    <link rel="shortcut icon" href="favicon.ico" />

    <meta name="description" content="">

    <!-- CSS -->
    <link href="assets/css/preload.css" rel="stylesheet">
    <link href="assets/css/vendors.css" rel="stylesheet">
    <link href="assets/css/style-orange2.css" rel="stylesheet" title="default">
    <link href="assets/css/style/style.css" rel="stylesheet" >
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
    <![endif]-->
</head>

<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<body>


<div id="sb-site">
<div class="boxed">


<?php
    $contactanos = true ;
    include_once 'menu.php' ;
?>

<header id="header-full">
    <div class="wrap-primary">

            <section class="carousel-section">
                <div id="carousel-example-generic" class="carousel carousel-razon slide" data-ride="carousel" data-interval="5000">


                    <!-- Wrapper for slides -->
                    <div class="carousel-inner ">

                        <div class="item active ">
                            <img src="assets/img/contactanos.png" alt="" class="img-responsive animated bounceInUp animation-delay-3 img-fondo-slider">
                            <div class="container">
                                <!-- <div class="row "> -->
                                    <div class="content-slider parent">
                                        <div class="carousel-caption children">
                                            <div class="carousel-text padd-left pull-left col-md-8">
                                                <div class="row">

                                                          <form class="form-horizontal col-md-12" role="form" method="POST" action="layout/mail-contact/mail-contact.php">

                                                              <input type="hidden" name="_token" value="nc3FcCCN9Uuy02nGcyOnX1RNedb8HgkMn1BW29e7">

                                                                <div class="form-group">
                                                                    <div class="col-md-8">
                                                                        <input type="text" class="form-control" name="nombre" required="" value="" placeholder="Nombres / Razón Social">
                                                                    </div>
                                                                </div>

                                                              <div class="form-group">
                                                                 <!-- <label class="col-md-4 control-label hidden-xs">Correo electrónico:</label> -->
                                                                 <div class="col-md-8">
                                                                    <input type="email" class="form-control" name="email" required="" value="" placeholder="Email">
                                                                 </div>
                                                              </div>

                                                              <div class="form-group">
                                                                 <!-- <label class="col-md-4 control-label hidden-xs">Celular:</label> -->
                                                                 <div class="col-md-8">
                                                                     <input type="tel" class="form-control" name="telefono" required="" value="" placeholder="Teléfono" maxlength="10" pattern="[7-9]{1}[0-9]{8,9}">
                                                                 </div>
                                                              </div>

                                                              <div class="form-group">
                                                                 <!-- <label class="col-md-4 control-label hidden-xs">Asunto:</label> -->
                                                                 <div class="col-md-8">
                                                                     <input type="text" class="form-control" name="asunto" required="" value="" placeholder="Asunto">
                                                                 </div>
                                                              </div>


                                                              <div class="form-group">
                                                                 <!-- <label class="col-md-4 control-label hidden-xs" for="mensaje">Mensaje:</label> -->
                                                                 <div class="col-md-8">
                                                                    <textarea class="form-control" name="mensaje" rows="5" id="mensaje" riquered="" placeholder="Mensaje"></textarea>
                                                                 </div>
                                                              </div>

                                                              <p class="clearfix"></p>

                                                              <div class="  form-group">
                                                                 <div class="col-md-8">
                                                                    <button type="submit" class="btn btn-ar btn-primary pull-right">
                                                                       Enviar                        </button>
                                                                 </div>
                                                              </div>
                                                           </form>



                                               </div>
                                           </div>
                                        </div>
                                    </div>

                                <!-- </div> -->
                            </div>
                        </div>


                    </div>


                </div>
            </section> <!-- carousel -->

    </div>
</header>

    <!-- <header class="main-header sm-header">
        <div class="container">
            <h4 class="page-title">
            <p class="text-center animated bounceInLeft animation-delay-12">Contáctanos </p></h4>

        </div>
    </header>
 -->

<?php include_once 'footer.php' ;?>

</div> <!-- boxed -->
</div> <!-- sb-site -->

<div id="back-top">
    <a href="#header"><i class="fa fa-chevron-up"></i></a>
</div>


<script src="assets/js/vendors.js"></script>

<script src="assets/js/styleswitcher.js"></script>

<!-- Syntaxhighlighter -->
<script src="assets/js/syntaxhighlighter/shCore.js"></script>
<script src="assets/js/syntaxhighlighter/shBrushXml.js"></script>
<script src="assets/js/syntaxhighlighter/shBrushJScript.js"></script>

<script src="assets/js/app.js"></script>
<script src="assets/js/index.js"></script>
<script src="assets/js/home_full.js"></script>
</body>


</html>
