
<nav class="navbar navbar-default navbar-dark yamm navbar-static-top navbar-header-full" role="navigation" id="header">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-bars"></i>
            </button>
            <a id="ar-brand" class="navbar-brand hidden-sm" href="/">Tiger<span>Soft</span></a>
        </div> <!-- navbar-header -->

        <?php
        ?>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
               <li class="<?php if(isset($index)) echo 'active' ;?> ">
                    <a href="index.php" class=" dropdown-toggle">Home</a>
                </li>

                <li class="<?php if(isset($nosotros)) echo 'active';?>">
                    <a href="nosotros.php" class="dropdown-toggle">Nosotros</a>
                </li>

                <li class="<?php if(isset($servicios)) echo 'active';?>" >
                    <a href="#services" class="dropdown-toggle">Servicios</a>
                </li>

                <li class="<?php if(isset($productos)) echo 'active';?>">
                    <a href="productos.php" class="dropdown-toggle">Productos</a>
                </li>

                <li class="<?php if(isset($contactanos)) echo 'active';?>">
                    <a href="contactanos.php" class="dropdown-toggle">Contactanos</a>
                </li>

                <!-- <li>
                    <a href="javascript:void(0);" class="dropdown-toggle">Blog</a>
                </li> -->
             </ul>
        </div><!-- navbar-collapse -->
    </div><!-- container -->
</nav>