<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from razonartificial.com/themes/reason/v1.4.5/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Nov 2015 14:35:09 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>Servicios de rastreo | Gps </title>

    <link rel="shortcut icon" href="favicon.ico" />

    <meta name="description" content="">

    <!-- CSS -->
    <link href="assets/css/preload.css" rel="stylesheet">
    <link href="assets/css/vendors.css" rel="stylesheet">
    <link href="assets/css/style-orange2.css" rel="stylesheet" title="default">
    <link href="assets/css/style/style.css" rel="stylesheet" >
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
    <![endif]-->
</head>

<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<body>


<div id="sb-site">
<div class="boxed">


<?php
    $productos = true ;
    include_once 'menu.php' ;
?>

    <header class="main-header sm-header">
        <div class="container">
            <h4 class="page-title">
                <p class="text-center animated bounceInLeft animation-delay-12">
                Nuestros Productos
                </p>
            </h4>

        </div>
    </header>



    <div class="container">
        <div class="row">
           <!--  <div class="col-xs-12">
                <div class="title-logo animated fadeInDown animation-delay-5">

                    <br>
                    <br>
                </div>
            </div> -->

            <div class="col-md-12">
                <article class="post animated fadeInLeft animation-delay-8">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3 class="post-title">
                            <a href="#" class="transicion">Cello-IQ</a>
                            </h3>
                            <div class="row">
                                <div class="col-lg-4">
                                <span class="   ">
                                    <img src="assets/img/productos/celloIQ.png" class=" img-post bg-none boder-none img-responsive" alt="Image">
                                </span>
                                </div>
                                <div class="col-lg-8 post-content">
                                    <p class="text-justify text-ml">
                                        <span>Cello-IQ</span> es una aplicación diseñada para la seguridad del conductor y eco-conducción, mejorando la seguridad de la flota y reduciendo los costos operacionales de la misma. Este equipo es uno de los muy pocos sistemas disponibles en el mercado que ofrecen una solución para cuidar la seguridad de su flota y su eco-conducción, y que simultáneamente se encuentra listo para ser integrado a cualquier plataforma telemática o de gestión de flotas con un mínimo esfuerzo de integración y desarrollo.
                                    </p>

                                    <a href="assets/pdf/productos/Cello-IQ.pdf" target="_blank" class="pull-right"> Descargue ficha técnica »</a>


                                </div>
                            </div>
                        </div>

                    </div>
                </article> <!-- post -->
            </div>

            <div class="col-md-12">
                <article class="post animated fadeInLeft animation-delay-8">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3 class="post-title">
                            <a href="#" class="transicion">Cello Track</a>
                            </h3>
                            <div class="row">
                                 <div class="col-lg-4 pull-right">
                                    <img src="assets/img/productos/cellotrack.png" class=" img-post bg-none boder-none img-responsive" alt="Image">
                                </div>

                                <div class="col-lg-8 post-content pull-left">
                                    <p class="text-justify text-ml">
                                        La línea de productos CelloTrack, diseñada para aplicaciones avanzadas de gestión y rastreo de bienes y basadas en sistemas de localización, proporciona una ampliada funcionalidad a través de la larga autonomía de sus productos, de aproximadamente tres años, fácil instalación y amplio abanico de aplicaciones disponibles.
                                    </p>
                                    <p class="text-justify text-ml" >
                                        Las capacidades ofrecidas por la Familia CelloTrack pueden reducir notablemente las pérdidas financieras de una empresa incurridas como resultado de la tradicionalmente difícil tarea de rastrear satisfactoriamente equipos tales como remolques, contenedores y trenes, entre otros.

                                    </p>
                                     <a href="assets/pdf/productos/CelloTrack-SPANISH.pdf" target="_blank" class="pull-right"> Descargue ficha técnica »</a>

                                </div>

                            </div>
                        </div>

                    </div>
                </article> <!-- post -->
            </div>

            <div class="col-md-12">
                <article class="post animated fadeInLeft animation-delay-8">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3 class="post-title">
                            <a href="#" class="transicion">CR300</a>
                            </h3>
                            <div class="row">
                                <div class="col-lg-4">
                                <span class="   ">
                                    <img src="assets/img/productos/CR300B.png" class=" img-post bg-none boder-none img-responsive" alt="Image">
                                </span>
                                </div>
                                <div class="col-lg-8 post-content">
                                    <p class="text-justify text-ml">
                                         El <span>CR300</span> es un dispositivo compacto para gestión de flotas y aplicaciones de seguridad, entre ellas recuperación de vehículos robados. Este dispositivo está disponible en dos versiones: CR300 y CR300B. Está dirigido a empresas con grandes flotas, compañías de seguros y compañías de arrendamiento entre otras.
                                    </p>
                                     <a href="assets/pdf/productos/CR300.pdf" target="_blank" class="pull-right"> Descargue ficha técnica »</a>

                                </div>
                            </div>
                        </div>
                        <!-- <div class="panel-footer post-info-b">
                            <div class="row">

                                <div class="col-sm-12">
                                    <a href="assets/pdf/productos/CR300.pdf" target="_blank" class="pull-right"> Descargue ficha técnica »</a>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </article> <!-- post -->
            </div>

            <div class="col-md-12">
                <article class="post animated fadeInLeft animation-delay-8">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3 class="post-title">
                            <a href="#" class="transicion">Tt8750+</a>
                            </h3>
                            <div class="row">
                                <div class="col-lg-4 pull-right">
                                    <img src="assets/img/productos/tt8750+.png" class=" img-post bg-none boder-none img-responsive" alt="Image">
                                </div>
                                <div class="col-lg-8 post-content pull-left">
                                    <p class="text-justify text-ml">
                                        El equipo Tt8750+ es un activo de bajo consumo de energía con Sensor de movimiento y batería de respaldo, contiene un Módem de 4 bandas, funciona con cualquier operador y/o red celular Resistente, de alta calidad y cubierta a prueba de salpicaduras,  Ultra Sensible (-16Dbl) ensamblado con antenas de alto poder, posee un conector opcional de SMA para la antena GPS.
                                    </p>
                                     <a href="assets/pdf/productos/TT8750+-Spanish-Rev05192014.pdf" target="_blank" class="pull-right"> Descargue ficha técnica »</a>

                                </div>
                            </div>
                        </div>
                       <!--  <div class="panel-footer post-info-b">
                            <div class="row">

                                <div class="col-sm-12">
                                    <a href="assets/pdf/productos/TT8750+-Spanish-Rev05192014.pdf" target="_blank" class="pull-right"> Descargue ficha técnica »</a>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </article> <!-- post -->
            </div>

            <div class="col-md-12">
                <article class="post animated fadeInLeft animation-delay-8">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3 class="post-title">
                            <a href="#" class="transicion">TT8850</a>
                            </h3>
                            <div class="row">

                                <div class="col-lg-4">
                                    <img src="assets/img/productos/tt8850-.png" class=" img-post bg-none boder-none img-responsive" alt="Image">

                                </div>

                                <div class="col-lg-8 post-content">
                                    <p class="text-justify text-ml">

                                       EL TT8850 , es un dispositivo perfecto para aplicaciones compactas , resistente al Agua, IPX5 Estándar, con Frecuencia de Banda Cuádruple: GSM/SMS/GPRS/TCP/UDP;  Con un GPS de Alta Precisión (uBlox Chipset),  Bajo Consumo de Energía. Reserva de hasta 25 días, Rastreo continuo entre 10 a 12 días con una sola carga de la batería Administración Eficiente de Energía, posees un protocolo Binario Optimizado que Minimiza los costos de transferencia de datos con sensor de movimiento en 3D para la detección de movimiento y la administración de Energía Color Encubierto, Botón de Pánico, 100% Portátil, Clip Magnético como Accesorio de Instalación (Opcional)
                                    </p>

                                    <a href="assets/pdf/productos/TT8850-Spanish-Rev1.1-051920141.pdf" target="_blank" class="pull-right"> Descargue ficha técnica »</a>

                                </div>


                            </div>
                        </div>
                        <!-- <div class="panel-footer post-info-b">
                            <div class="row">

                                <div class="col-sm-12">
                                    <a href="assets/pdf/productos/TT8850-Spanish-Rev1.1-051920141.pdf" target="_blank" class="pull-right"> Descargue ficha técnica »</a>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </article> <!-- post -->
            </div>


        </div>
    </div> <!-- container -->


<?php include_once 'footer.php' ;?>

</div> <!-- boxed -->
</div> <!-- sb-site -->

<div id="back-top">
    <a href="#header"><i class="fa fa-chevron-up"></i></a>
</div>


<script src="assets/js/vendors.js"></script>

<script src="assets/js/styleswitcher.js"></script>

<!-- Syntaxhighlighter -->
<script src="assets/js/syntaxhighlighter/shCore.js"></script>
<script src="assets/js/syntaxhighlighter/shBrushXml.js"></script>
<script src="assets/js/syntaxhighlighter/shBrushJScript.js"></script>

<script src="assets/js/app.js"></script>
<script src="assets/js/index.js"></script>
<script src="assets/js/home_full.js"></script>
</body>


</html>
