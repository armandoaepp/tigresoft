<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from razonartificial.com/themes/reason/v1.4.5/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Nov 2015 14:35:09 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>Servicios de rastreo | Gps </title>

    <link rel="shortcut icon" href="favicon.ico" />

    <meta name="description" content="">

    <!-- CSS -->
    <link href="assets/css/preload.css" rel="stylesheet">
    <link href="assets/css/vendors.css" rel="stylesheet">
    <link href="assets/css/style-orange2.css" rel="stylesheet" title="default">
    <link href="assets/css/style/style.css" rel="stylesheet" >
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
    <![endif]-->
</head>

<!-- Preloader -->
<!-- <div id="preloader">
    <div id="status">&nbsp;</div>
</div> -->

<body>


<div id="sb-site">
<div class="boxed">


<?php
  $index = true ;
  include_once 'menu.php' ;
?>

<header id="header-full">
    <div class="wrap-primary">

            <section class="carousel-section">
                <div id="carousel-example-generic" class="carousel carousel-razon slide" data-ride="carousel" data-interval="5000">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="5"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner ">

                        <div class="item active ">
                            <img src="assets/img/slider/tigersoft-4.png" alt="" class="img-responsive animated bounceInUp animation-delay-3 img-fondo-slider">
                            <div class="container">

                                    <div class="content-slider parent">
                                        <div class="carousel-caption children  ">
                                            <div class="carousel-text padd-left pull-center ">
                                               <h1 class="animated fadeInDownBig animation-delay-1 carousel-title color-black">
                                                Gestion de Software
                                               </h1>

                                               <ul class="list-unstyled carousel-list text-xlg bold-normal color-black list-style-2">
                                                    <li class="animated bounceInLeft animation-delay-10  ">
                                                        <i class="fa fa-angle-double-right "></i>
                                                       Proyectos de Software
                                                    </li>
                                                    <li class="animated bounceInLeft animation-delay-13 ">
                                                        <i class="fa fa-angle-double-right "></i>
                                                        Construcción de App's
                                                    </li>
                                                    <li class="animated bounceInLeft animation-delay-16 ">
                                                        <i class="fa fa-angle-double-right "></i>
                                                        Facturación Electronica
                                                    </li>
                                                    <li class="animated bounceInLeft animation-delay-19 ">
                                                        <i class="fa fa-angle-double-right "></i>
                                                        Elaboración de MVP's
                                                    </li>
                                                     <li class="animated bounceInLeft animation-delay-21 ">
                                                        <i class="fa fa-angle-double-right "></i>
                                                        Sistemas Biometricos
                                                    </li>

                                                     <li class="animated bounceInLeft animation-delay-24 ">
                                                        <i class="fa fa-angle-double-right "></i>
                                                        Sistemas a Medida
                                                    </li>
                                               </ul>

                                           </div>
                                        </div>
                                    </div>


                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/img/slider/tigersoft-5.png" alt="" class="img-responsive animated bounceInUp animation-delay-3 img-fondo-slider">
                            <div class="container">

                                    <div class="content-slider ">
                                        <div class="carousel-caption   ">
                                            <div class="carousel-text padd-left pull-center ">
                                            <br>


                                            <p class="animated fadeInUpBig animation-delay-1 pull-right ">
                                              <span class="price-box bg-caja-slider">Equipos Satelitales</span>
                                            </p>

                                            <br>
                                            <br>

                                            <p class="animated fadeInUpBig animation-delay-5 ">
                                              <span class="price-box bg-caja-slider">Infraestructura</span>
                                            </p>
                                            <br>
                                            <br>
                                            <br>

                                            <p class="animated fadeInUpBig animation-delay-10  text-center">
                                              <span class="price-box bg-caja-slider">Sistemas de Telemetria</span>
                                            </p>
                                            <br>
                                            <br>
                                            <br>


                                            <p class="animated fadeInUpBig animation-delay-15 ">
                                              <span class="price-box bg-caja-slider">Redes IPV6</span>
                                            </p>
                                            <br>
                                            <br>

                                            <p class="animated fadeInUpBig animation-delay-20  pull-right   ">
                                              <span class="price-box bg-caja-slider">Telecomunicaciones</span>

                                            </p>


                                           </div>
                                        </div>
                                    </div>


                            </div>
                        </div>

                        <div class="item ">
                            <img src="assets/img/slider/tigersoft-1.jpg" alt="" class="img-responsive animated bounceInUp animation-delay-3 img-fondo-slider">
                            <div class="container">

                                    <div class="content-slider parent">
                                        <div class="carousel-caption children">
                                            <div class="carousel-text padd-left pull-center">
                                                <p></p>
                                               <h1 class="animated fadeInDownBig animation-delay-7 carousel-title">
                                               Soluciones a tu medida
                                               </h1>

                                               <ul class="list-unstyled carousel-list ">
                                                 <li class="animated bounceInLeft animation-delay-11">
                                                        <i class="fa fa-check fs-1x-xs"></i>
                                                       Sistema de transmisión continua
                                                    </li>
                                                    <li class="animated bounceInLeft animation-delay-11">
                                                        <i class="fa fa-check fs-1x-xs"></i>
                                                        Alerta las 24 horas del día
                                                    </li>
                                                       <li class="animated bounceInLeft animation-delay-13">
                                                        <i class="fa fa-check fs-1x-xs"></i>
                                                        Control de Velocidad
                                                    </li>
                                                       <li class="animated bounceInLeft animation-delay-15">
                                                        <i class="fa fa-check fs-1x-xs"></i>
                                                        Creaccion de limites de Geo-Cercas
                                                    </li>
                                                    </li>
                                                       <li class="animated bounceInLeft animation-delay-15">
                                                        <i class="fa fa-check fs-1x-xs"></i>
                                                        Los mejores equipos del Mercado
                                                    </li>
                                               </ul>

                                           </div>
                                        </div>
                                    </div>


                            </div>
                        </div>

                        <div class="item  ">
                            <img src="assets/img/slider/tigersoft-2.jpg" alt="" class="img-responsive animated bounceInUp animation-delay-3 img-fondo-slider">
                            <div class="container">

                                    <div class="content-slider parent">
                                        <div class="carousel-caption children">
                                            <div class="carousel-text padd-left pull-right">
                                                <p></p>
                                               <h1 class="animated fadeInDownBig animation-delay-7 carousel-title ">
                                               Soluciones a tu medida
                                               </h1>

                                               <ul class="list-unstyled carousel-list">
                                                 <li class="animated bounceInLeft animation-delay-11">
                                                        <i class="fa fa-angle-double-right fs-1x-xs"></i>
                                                       Sistema de transmisión continua
                                                    </li>
                                                    <li class="animated bounceInLeft animation-delay-11">
                                                        <i class="fa fa-angle-double-right fs-1x-xs"></i>
                                                        Alerta las 24 horas del día
                                                    </li>
                                                       <li class="animated bounceInLeft animation-delay-13">
                                                        <i class="fa fa-angle-double-right fs-1x-xs"></i>
                                                        Control de Velocidad
                                                    </li>
                                                       <li class="animated bounceInLeft animation-delay-15">
                                                        <i class="fa fa-angle-double-right fs-1x-xs"></i>
                                                        Creaccion de limites de Geo-Cercas
                                                    </li>
                                                    </li>
                                                       <li class="animated bounceInLeft animation-delay-15">
                                                        <i class="fa fa-angle-double-right fs-1x-xs"></i>
                                                        Los mejores equipos del Mercado
                                                    </li>
                                               </ul>

                                           </div>
                                        </div>
                                    </div>


                            </div>
                        </div>

                        <div class="item  ">
                            <img src="assets/img/slider/tigersoft-3.jpg" alt="" class="img-responsive animated bounceInUp animation-delay-3 img-fondo-slider">
                            <div class="container">

                                    <div class="content-slider parent">
                                        <div class="carousel-caption children">
                                            <div class="carousel-text padd-left pull-right">
                                                <p></p>
                                               <h1 class="animated fadeInDownBig animation-delay-7 carousel-title">
                                               Soluciones a tu medida
                                               </h1>

                                               <ul class="list-unstyled carousel-list ">
                                                 <li class="animated bounceInLeft animation-delay-11">
                                                        <i class="fa fa-check fs-1x-xs"></i>
                                                       Sistema de transmisión continua
                                                    </li>
                                                    <li class="animated bounceInLeft animation-delay-11">
                                                        <i class="fa fa-check fs-1x-xs"></i>
                                                        Alerta las 24 horas del día
                                                    </li>
                                                       <li class="animated bounceInLeft animation-delay-13">
                                                        <i class="fa fa-check fs-1x-xs"></i>
                                                        Control de Velocidad
                                                    </li>
                                                       <li class="animated bounceInLeft animation-delay-15">
                                                        <i class="fa fa-check fs-1x-xs"></i>
                                                        Creaccion de limites de Geo-Cercas
                                                    </li>
                                                    </li>
                                                       <li class="animated bounceInLeft animation-delay-15">
                                                        <i class="fa fa-check fs-1x-xs"></i>
                                                        Los mejores equipos del Mercado
                                                    </li>
                                               </ul>

                                           </div>
                                        </div>
                                    </div>


                            </div>
                        </div>

                        <div class="item  ">
                            <img src="assets/img/slider/tigersoft-6.png" alt="" class="img-responsive animated bounceInUp animation-delay-3 img-fondo-slider">
                            <div class="container">

                                    <div class="content-slider parent">
                                        <div class="carousel-caption children">
                                            <div class="carousel-text padd-left">
                                                <p></p>
                                               <h1 class="animated fadeInDownBig animation-delay-7 carousel-title color-black">
                                               Pronto Rastreo Maritimo y Fluvial
                                               </h1>
                                               <br>
                                               <br>
                                               <br>
                                               <br>
                                               <br>
                                               <br>
                                               <br>
                                               <br>
                                               <br>
                                               <br>
                                               <br>
                                               <br>



                                           </div>
                                        </div>
                                    </div>


                            </div>
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
            </section> <!-- carousel -->

    </div>
</header>

<section class="margin-bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="content-box box-default animated fadeInUp animation-delay-10">
                    <span class="icon-ar icon-ar-xxl-lg icon-ar-round icon-ar-inverse">
                    <!-- <img src="assets/img/service/icon-service.png" alt=""> -->
                    <div class="icon-service"></div>
                    <!-- <i class="fa fa-map-marker"></i> -->
                    </span>
                    <h4 class="content-box-title">Servicio Básico GPS</h4>
                    <p>
                        Sistema de transmisión no continua, y bajo demanda, elección de equipos según la necesidad y control de unidades particulares.<a href="servicios.html#basico"> ver mas</a>
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="content-box box-default animated fadeInUp animation-delay-14">
                    <span class="icon-ar icon-ar-xxl-lg icon-ar-round icon-ar-inverse">
                    <!-- <i class="fa fa-map-marker"></i> -->
                    <img src="assets/img/service/servicio-2.png" alt="">

                    </span>
                    <h4 class="content-box-title">Servicio Full GPS</h4>
                    <p>
                        Servicio diseñado para la administración de Flotas vehículares, que permite el monitoreo continuo, los 365/12/24 del... <a href="servicios.html#full"> ver mas</a>
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="content-box box-default animated fadeInUp animation-delay-16">
                    <span class="icon-ar icon-ar-xxl-lg icon-ar-round icon-ar-inverse">
                    <img src="assets/img/service/servicio-3.png" alt="">
                    <!-- <div class="icon-service"></div> -->
                    </span>
                    <h4 class="content-box-title"> Seguimiento de Activos</h4>
                    <p>
                         Sistema Ideal para el control de activos que necesitan trasladarse de una ciudad a otra, con equipos de sumministro  de  ... <a href="servicios.html#activo"> ver mas</a>
                    </p>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="margin-bottom">
    <div class="container">
        <h2 class="section-title">Nuestros Productos</h2>
        <div class="bxslider-controls">
          <span id="bx-prev4"><a class="bx-prev" href="">&lt;</a></span>
          <span id="bx-next4"><a class="bx-next" href="">&gt;</a></span>
        </div>
        <div class="bx-wrapper" >
          <div class="bx-viewport" aria-live="polite" >
             <ul class="bxslider" id="latest-works" >



                <li  class="bx-clone" aria-hidden="true">
                   <div class="img-caption-ar">
                      <img src="assets/img/productos/celloIQ.png" class="img-responsive" alt="Image">
                      <div class="caption-ar">
                         <div class="caption-content">
                            <a href="#" class="animated fadeInDown">
                            <i class="fa fa-search"></i>Más Información</a>
                            <h4 class="caption-title">Cello IQ40</h4>
                         </div>
                      </div>
                   </div>
                </li>

                <li  class="bx-clone" aria-hidden="true">
                   <div class="img-caption-ar">
                      <img src="assets/img/productos/CR300B.png" class="img-responsive" alt="Image">
                      <div class="caption-ar">
                         <div class="caption-content">
                            <a href="#" class="animated fadeInDown">
                            <i class="fa fa-search"></i>Más Información</a>
                            <h4 class="caption-title">CR300B</h4>
                         </div>
                      </div>
                   </div>
                </li>



                <li  class="bx-clone" aria-hidden="true">
                   <div class="img-caption-ar ">
                      <img src="assets/img/productos/cellotrack.png" style="width: 100px;"class=" centered img-responsive" alt="Image">
                      <div class="caption-ar">
                         <div class="caption-content">
                            <a href="#" class="animated fadeInDown"><i class="fa fa-search"></i>Más Información</a>
                            <h4 class="caption-title">Cello Truck</h4>
                         </div>
                      </div>
                   </div>
                </li>

                <li  class="bx-clone" aria-hidden="true">
                   <div class="img-caption-ar">
                      <img src="assets/img/productos/tt8750+.png" class="img-responsive" alt="Image">
                      <div class="caption-ar">
                         <div class="caption-content">
                            <a href="#" class="animated fadeInDown">
                            <i class="fa fa-search"></i>Más Información</a>
                            <h4 class="caption-title">TT8750+</h4>
                         </div>
                      </div>
                   </div>
                </li>

                <li  class="bx-clone" aria-hidden="true">
                   <div class="img-caption-ar">
                      <img src="assets/img/productos/tt8850-.png" class="img-responsive" alt="Image">
                      <div class="caption-ar">
                         <div class="caption-content">
                            <a href="#" class="animated fadeInDown">
                            <i class="fa fa-search"></i>Más Información</a>
                            <h4 class="caption-title">TT8850</h4>
                         </div>
                      </div>
                   </div>
                </li>

             </ul>
          </div>
          <div class="bx-controls"></div>
        </div>
    </div>
</section>

<?php include_once 'footer.php' ;?>

</div> <!-- boxed -->
</div> <!-- sb-site -->

<div id="back-top">
    <a href="#header"><i class="fa fa-chevron-up"></i></a>
</div>


<script src="assets/js/vendors.js"></script>

<script src="assets/js/styleswitcher.js"></script>

<!-- Syntaxhighlighter -->
<script src="assets/js/syntaxhighlighter/shCore.js"></script>
<script src="assets/js/syntaxhighlighter/shBrushXml.js"></script>
<script src="assets/js/syntaxhighlighter/shBrushJScript.js"></script>

<script src="assets/js/app.js"></script>
<script src="assets/js/index.js"></script>
<script src="assets/js/home_full.js"></script>
</body>


</html>
