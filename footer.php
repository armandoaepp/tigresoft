<aside id="footer-widgets">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <h3 class="footer-widget-title">Contactanos</h3>
                <ul class="list-unstyled ">
                    <li>Tel.:(+51 074) 233187</li>
                    <li>RPM: #971257642</li>
                    <li>RPC: 958795034</li>
                    <li>administracion@corptigersoft.com.pe</li>
                </ul>

            </div>

            <div class="col-md-2 pull-right">
                <h3 class="footer-widget-title">Siguenos en: </h3>
                <ul class="list-unstyled ">
                    <li><a href="https://www.facebook.com/Tigersoftsac/">Facebook</a></li>
                    <li><a href="https://twitter.com/Tigersoftsac">Twitter</a></li>
                </ul>

            </div>

        </div> <!-- row -->
    </div> <!-- container -->
</aside>

<footer id="footer">
    <p> <a href="http://www.corptigersoft.com.pe/" >TeSoft</a> &reg; 2015, Todos los derechos reservados.</p>
</footer>