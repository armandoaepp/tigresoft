<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from razonartificial.com/themes/reason/v1.4.5/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Nov 2015 14:35:09 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>Servicios de rastreo | Gps </title>

    <link rel="shortcut icon" href="favicon.ico" />

    <meta name="description" content="">

    <!-- CSS -->
    <link href="assets/css/preload.css" rel="stylesheet">
    <link href="assets/css/vendors.css" rel="stylesheet">
    <link href="assets/css/style-orange2.css" rel="stylesheet" title="default">
    <link href="assets/css/style/style.css" rel="stylesheet" >
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
    <![endif]-->
</head>

<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<body>


<div id="sb-site">
<div class="boxed">


<?php
    $nosotros = true ;
    include_once 'menu.php' ;
?>

    <header class="main-header sm-header">
        <div class="container">
            <h4 class="page-title"><p class="text-center animated bounceInLeft animation-delay-12">Nosotros </p></h4>

        </div>
    </header>



    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="title-logo animated fadeInDown animation-delay-5">

                    <br>
                    <br>
                </div>
            </div>
            <div class="col-md-12">
                <h1 class="right-line no-margin-top">Nuestra Empresa</h1>
                <p>
                <span class="color-orange">TIGERSOFT</span> , es una empresa Peruana especializada en el desarrollo de sistemas de integración, implementando soluciones a medida, entre ellos Sistemas de gestión, ERP´s, Integracion modular de sistemas Contables, Sistemas de Rastreo GPS, entreo otros.
                </p>


                <p>
                    <span class="color-orange">TIGERSOFT</span> Cuenta con autorización del MTC a través del permiso de Casa Comercializadora de equipos y Aparatos de Telecomunicaciones, así mismo cuenta con el respaldo de la Firma Israelí Pointer Ltda, para el uso y distribución de los Equipos GPS de alta Gama Cello F, IQ40, IQ50, entre otros.  Nuestra filosofía de trabajo es brindar a cada uno de nuestros usuarios herramientas tecnológicas capaces de responder ante eventualidades de siniestro
                </p>

                <h2 class="section-title">Misión</h2>
                <p>
                     Brindar soluciones de alta gama ofreciendo equipos de alta tecnología, administrables y homologados en el mercado nacional y regional.
                     <br>

                </p>

                <h2 class="section-title">Visión</h2>
                <p>
                     Liderar el mercado Regional, en distribución, comercialización e instalación de equipos AVL/GPS/GPRS.
                     <br>

                </p>

                <h2 class="section-title">Valores</h2>
                <p>
                     Nuestro personal, busca siempre llevar cada tarea seguido de nuestros valores: la Innovación, Integridad, Confianza, Respeto.
                     <br>

                </p>

                <h2 class="section-title">Fortalezas</h2>
                <p>
                    Nuestras principales <span class="color-orange">Fortalezas</span> están basadas en la Tecnología, Experiencia y Flexibilidad.

                </p>
                <br>
                <br>



            </div>


        </div>
    </div> <!-- container -->

<?php include_once 'footer.php' ;?>

</div> <!-- boxed -->
</div> <!-- sb-site -->

<div id="back-top">
    <a href="#header"><i class="fa fa-chevron-up"></i></a>
</div>


<script src="assets/js/vendors.js"></script>

<script src="assets/js/styleswitcher.js"></script>

<!-- Syntaxhighlighter -->
<script src="assets/js/syntaxhighlighter/shCore.js"></script>
<script src="assets/js/syntaxhighlighter/shBrushXml.js"></script>
<script src="assets/js/syntaxhighlighter/shBrushJScript.js"></script>

<script src="assets/js/app.js"></script>
<script src="assets/js/index.js"></script>
<script src="assets/js/home_full.js"></script>
</body>


</html>
